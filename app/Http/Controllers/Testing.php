<?php
/**
 * Created by PhpStorm.
 * User: alejandrovanhoutte
 * Date: 12/11/15
 * Time: 17:04
 */

namespace App\Http\Controllers;


use Illuminate\Support\Facades\View;

class Testing extends Controller {

    public $restful = true;

    public function get_index(){
        return View::make('testing.index');
    }
}