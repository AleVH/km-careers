<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('contacts')->insert(array(
            'name'          => 'Marc',
            'email'         => 'm.buenaventura@karenmillen.co.uk',
            'location'      => 1,
            'canLogin'      => 1,
            'roles'         => '1,2,3,4,5,6',
            'created_at'    => date('Y-m-d H:m:s')
        ));

        DB::table('contacts')->insert(array(
            'name'          => 'Ale',
            'email'         => 'a.vanhoutte@karenmillen.co.uk',
            'location'      => 1,
            'canLogin'      => 1,
            'roles'         => '1,2,3,4,5,6',
            'created_at'    => date('Y-m-d H:m:s')
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('contacts')->where('name', '=', 'Marc')->delete();
        DB::table('contacts')->where('name', '=', 'Ale')->delete();
    }
}
